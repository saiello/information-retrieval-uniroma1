
Web Analyzer
UniversitÓ di Roma La Sapienza
Autori:
	Aiello Simone
	Andreocci Valerio
	Cialfi Tiziano
	
05/11/2012

HOW TO EXEC:
	> mvn install
	> cd trend-analyzer/target
	> echo spending review > input.txt
	
	Windows users:
	> run.bat input.txt

	Unix users:
	> ./run.sh input.txt
