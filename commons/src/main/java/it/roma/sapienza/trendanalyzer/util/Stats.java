package it.roma.sapienza.trendanalyzer.util;

import org.apache.commons.math.stat.StatUtils;

/**
 * 
 * @author Simone
 *
 */
public class Stats  {

	/**
	 * @param data
	 * @return
	 */
	public static double mean(double[] data) {
		return StatUtils.mean(data);
	}

	/**
	 * @param data
	 * @return
	 */
	public static double standardDeviation(double[] data) {
		return Math.sqrt(StatUtils.variance(data));
	}
    
} 
