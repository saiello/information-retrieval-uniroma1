package it.roma.sapienza.snippetsdownloader;

import it.roma.sapienza.snippetsdownloader.PageResponseHandler.Page;

import java.io.IOException;
import java.util.Collection;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Classe astratta che implementa {@link SnippetCollector}
 * @author Simone
 * */
public abstract class AbstractSnippetCollector implements SnippetCollector {
	
	private static final Logger logger = LoggerFactory
		.getLogger(AbstractSnippetCollector.class);

	private final SnippetWriter snippetWriter;
	
	private final SnippetParser snippetParser;
	
	private final PageResponseHandler pageResponseHandler;
	
	private DefaultHttpClient httpclient;
	
	/**
	 * @param snippetWriter - l'implementazione di {@link SnippetWriter} da utilizzare
	 */
	public AbstractSnippetCollector(SnippetWriter snippetWriter) {
		this.snippetWriter = snippetWriter;
		this.snippetParser = new SnippetParser();
		this.pageResponseHandler = new PageResponseHandler();
	}
	
	/**
	 * {@inheritDoc }
	 * */
	public void collect(String term) {
		//start the http client
		httpclient = new DefaultHttpClient();
		
		Collection<Snippet> snippets = downloadSnippets(term);
		if(snippets!=null){
			for (Snippet snippet : snippets) {
				snippetWriter.write(snippet);
			}
		}
		try {
			snippetWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected abstract Collection<Snippet> downloadSnippets(String term);
	
	protected Collection<Snippet> extractSnippets(Page page, String term){
		return snippetParser.parseSnippets(page,term);
	}
	
	/**
	 * Effettua una richiesta GET verso l'url passato in input e restituisce l'output dell'apposito ResponseHandler fornito come parametro.
	 * @param url - l'url verso cui effettuare la richiesta GET
	 * @param handler - Il {@link ResponseHandler} da utilizzare per manipolare la response della chiamata GET
	 * @author Simone
	 * */
	protected <E>E doGet(String url,ResponseHandler<E> handler){
		E out = null;
		
		try {
			out = httpclient.execute(new HttpGet(url), handler);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}
	
	/**
	 * Restituisce la {@link Page} corrispondente all'url passato in input
	 * @author Simone 
	 * @param url - l'url identificativo della pagina da scaricare
	 * @return Page
	 * */
	protected Page fetchPage(String url) {
		logger.info("Fetching page [ url: {} ]", url);

		Page page = null;
		try{
			HttpGet httpget = new HttpGet(url);
			page = httpclient.execute(httpget, pageResponseHandler);
			page.setUrl(url);
		}catch (Exception e) {
			logger.error("Error fetching the page corresponding to: "+url);
		}
		return page;
	}

}
