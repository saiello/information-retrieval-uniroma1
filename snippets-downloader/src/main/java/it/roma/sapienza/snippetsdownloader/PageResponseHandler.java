package it.roma.sapienza.snippetsdownloader;

import it.roma.sapienza.snippetsdownloader.PageResponseHandler.Page;
import it.roma.sapienza.snippetsdownloader.utils.AnchorRecognizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementazione di {@link ResponseHandler}.
 * Restituisce un oggetto di tipo {@link Page} che rappresenta l'HttpResponse
 * */
public class PageResponseHandler implements ResponseHandler<Page> {
	
	private static final Logger logger = LoggerFactory
		.getLogger(PageResponseHandler.class);
	
	final Page EMPTY_PAGE = new Page();
	{
		EMPTY_PAGE.links = new ArrayList<String>();
	}
	
	final AnchorRecognizer ANCHOR_RECOGNIZER = new AnchorRecognizer();

	public Page handleResponse(HttpResponse arg0)throws ClientProtocolException, IOException {
		
		Page page = EMPTY_PAGE;
		StatusLine statusLine = arg0.getStatusLine();
		logger.debug(statusLine.toString());
		if(statusLine.getStatusCode()==200){
			InputStream inputStream = arg0.getEntity().getContent();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			Set<String> urls = new HashSet<String>();
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				urls.addAll(ANCHOR_RECOGNIZER.recognize(line));
				sb.append(line);
			}
			page = new Page();
			page.links = urls;
			page.text = Jsoup.parse(sb.toString()).text();
		}else{
			arg0.getEntity().getContent();
		}
		return page;
	}
	
	/**
	 * Modello di una pagina
	 * */
	public static class Page{
		
		/**
		 * L'url della pagina rappresentata
		 * */
		private String url;
		
		/**
		 * Tutti gli url contenuti negli anchor presenti all'interno della pagina 
		 * */
		private Collection<String> links;
		
		/**
		 * Il testo della pagina
		 * */
		private String text;
		

		/**
		 * @return i link contenuti nella pagina
		 */
		public Collection<String> getLinks() {
			return links;
		}
		
		/**
		 * @return il testo della pagina
		 */
		public String getText() {
			return text;
		}
		
		/**
		 * @return l'url che identifica la pagina
		 */
		public String getUrl() {
			return url;
		}

		/**
		 * @param url - l'url che identifica la pagina
		 */
		public void setUrl(String url) {
			this.url = url;			
		}

		
		
	}
	
}