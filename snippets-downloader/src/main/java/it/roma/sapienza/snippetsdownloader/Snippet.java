package it.roma.sapienza.snippetsdownloader;

/**
 * @author Simone
 * Classe che modella gli snippet
 * */
public class Snippet {

	/**
	 * Url sorgente dello snippet
	 * */
	private final String url;
	
	/**
	 * Testo dello snippet
	 * */
	private final String text;
	
	/**
	 * Termine a cui fa riferimento lo snippet
	 * */
	private final String term;
	
	/**
	 * 
	 * @param url - url sorgente dello snippet
	 * @param text - testo dello snippet
	 * @param term - termine a cui fa riferimento lo snippet
	 */
	public Snippet(String url,String text, String term) {
		this.url = url;
		this.text = text;
		this.term = term;
	}
	
	/**
	 * @return String - termine a cui fa riferimento lo snippet
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * @return String - testo dello snippet
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return String - url sorgente
	 */
	public String getUrl() {
		return url;
	}

	@Override
	public String toString() {
		return "Snippet [url=" + url + ", text=" + text + "]";
	}
	
	
	

}
