package it.roma.sapienza.snippetsdownloader;

/**
 * @author Simone
 *
 */
public interface SnippetCollector {
	
	/**
	 * Colleziona tutti gli snippet relativi al termine fornito in input
	 * @param term - il termine per cui collezionare gli snippet
	 * */
	void collect(String term);

}
