package it.roma.sapienza.snippetsdownloader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Implementa l'interfaccia {@link SnippetWriter} scrivendo su fileSystem
 * @author Simone
 * */
public class SnippetFileWriter implements SnippetWriter {
	
	private int counter;
	
	private PrintWriter printWriter;
	
	/**
	 * @param filename - nome del file di output su cui verranno scritti gli snippets
	 * @author Simone
	 * */
	public SnippetFileWriter(String filename) {
		counter = 1;
		try {
			printWriter = new PrintWriter(filename);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("File not found.");
		}
	}

	/**
	 * {@inheritDoc}
	 * */
	public synchronized void write(Snippet snippet) {
		printWriter.println(String.format("#%d : %s",counter++,snippet.getUrl()));
		printWriter.println(snippet.getText()+"\n");
	}


	public void close() throws IOException {
		printWriter.close();
	}

	public void flush() throws IOException {
		printWriter.flush();
	}

}
