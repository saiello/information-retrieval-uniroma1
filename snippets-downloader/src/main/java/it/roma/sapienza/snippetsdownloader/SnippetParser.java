package it.roma.sapienza.snippetsdownloader;

import it.roma.sapienza.snippetsdownloader.PageResponseHandler.Page;
import it.roma.sapienza.snippetsdownloader.utils.IMSentenceFragmenter;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.util.Version;


/**
 * Parser di snippet
 * @author Simone
 * */
public class SnippetParser {
	
    private static final int MAX_NUMBER_OF_FRAGMENT = 5;
	
    private Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
    
    private QueryParser parser = new QueryParser(Version.LUCENE_30, "", analyzer);
   
    
    /**
     * Provvede ad effettuare il parsing della pagina fornita in input, restituendo tutti gli {@link Snippet} che contengono il termine specificato.
     * @param page - pagina da cui estrarre gli snippet
     * @param term - termine per cui estrarre gli snippet
     * @param maxNumberOfSnippet - numero massimo di snippet da estrarre
     * @return Collection<Snippet>
     * */
    public Collection<Snippet> parseSnippets(Page page, String term, int maxNumberOfSnippet) {
    	List<Snippet> out = new ArrayList<Snippet>();
		
    	if(page.getText()!=null){
    		QueryScorer scorer;
    		try {
    			scorer = new QueryScorer(parser.parse(term));
    		
    		    IMSentenceFragmenter fragmenter = new IMSentenceFragmenter();
    		    scorer.setExpandMultiTermQuery(true);
    		    
    		    Highlighter highlighter = new Highlighter(scorer);
    		    highlighter.setTextFragmenter(fragmenter);
//    		    highlighter.setEncoder(new SimpleHTMLEncoder());
    		    TokenStream tokenStream = analyzer.reusableTokenStream("", new StringReader(page.getText()));
    		    String[] fragments = highlighter.getBestFragments(tokenStream,page.getText(),maxNumberOfSnippet);
    		    for (String string : fragments) {
    				out.add(new Snippet(page.getUrl(),string,term));
    			}
        
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
		return out;
    }
    
    /**
     * @param page - pagina da cui estrarre gli snippet
     * @param term - termine per cui estrarre gli snippet
     * @return Collection<Snippet>
     * */
	public Collection<Snippet> parseSnippets(Page page, String term) {
		return parseSnippets(page, term, MAX_NUMBER_OF_FRAGMENT);
	}

}
