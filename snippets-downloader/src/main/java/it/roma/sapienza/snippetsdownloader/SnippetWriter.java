package it.roma.sapienza.snippetsdownloader;

import java.io.Closeable;
import java.io.Flushable;

/**
 * Interfaccia SnippetWriter.
 * @author Simone  
 * */
public interface SnippetWriter extends Closeable,Flushable {

	/**
	 * Dovr� implementare la scrittura dello snippet secondo le logiche proprie del writer
	 * @param snippet - lo snippet da scrivere
	 * @author Simone
	 * */
	public void write(Snippet snippet);

}
