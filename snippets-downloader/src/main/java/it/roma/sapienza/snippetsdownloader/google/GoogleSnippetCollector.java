/**
 * 
 */
package it.roma.sapienza.snippetsdownloader.google;

import it.roma.sapienza.snippetsdownloader.AbstractSnippetCollector;
import it.roma.sapienza.snippetsdownloader.PageResponseHandler.Page;
import it.roma.sapienza.snippetsdownloader.Snippet;
import it.roma.sapienza.snippetsdownloader.SnippetCollector;
import it.roma.sapienza.snippetsdownloader.SnippetWriter;
import it.roma.sapienza.snippetsdownloader.google.GoogleSnippetCollector.GoogleResult.Result;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

/**
 * Implementazione dell'interfaccia SnippetCollector.
 * Gli snippet vengono recuperati dalle pagine restituite dalla ricerca eseguita sul motore di ricerca Google.
 * @author Simone
 * 
 */
public class GoogleSnippetCollector extends AbstractSnippetCollector implements
		SnippetCollector {

	private static final int GOOGLE_RESULTS_INCREMENTS = 4;

	private static final int MAX_GOOGLE_RESULTS_NUMBER = 10;

	private static final String BASE_URL = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=%s&start=%d";

	private static final Logger logger = LoggerFactory
			.getLogger(GoogleSnippetCollector.class);


	private final GoogleResultHandler googleResultHandler;

	/**
	 * @param snippetWriter - l'implementazione di {@link SnippetWriter} da utilizzare
	 */
	public GoogleSnippetCollector(SnippetWriter snippetWriter) {
		super(snippetWriter);
		googleResultHandler = new GoogleResultHandler();
	}

	@Override
	protected Collection<Snippet> downloadSnippets(String term) {
		logger.info("collect snippets for term [ {} ]", term);

		List<Snippet> output = new ArrayList<Snippet>();

		term = term.replaceAll(" ", "+");

		List<Result> results = new ArrayList<Result>();

		for (int i = 0; i <= MAX_GOOGLE_RESULTS_NUMBER; i += GOOGLE_RESULTS_INCREMENTS) {
			String url = String.format(BASE_URL, term, i);
			logger.debug(url);
			GoogleResult googleResult = doGet(url, googleResultHandler);
			if(googleResult!=null)
				results.addAll(googleResult.getResponseData().getResults());
		}
		
		for (Result r : results) {
			Page page = fetchPage(r.getUrl());
			if(page!=null)
				output.addAll(extractSnippets(page, term));
		}

		return output;

	}


	/**
	 * Handler per la responde del servizio di ricerca di Google
	 * @author Simone 
	 * */
	static class GoogleResultHandler implements ResponseHandler<GoogleResult> {

		final GoogleResult EMPTY_GOOGLE_RESULT = new GoogleResult();
		{
			EMPTY_GOOGLE_RESULT.responseData = new GoogleResult.ResponseData();
			EMPTY_GOOGLE_RESULT.responseData.results = new ArrayList<GoogleSnippetCollector.GoogleResult.Result>();
		}

		public GoogleResult handleResponse(HttpResponse arg0)
				throws ClientProtocolException, IOException {
			StatusLine statusLine = arg0.getStatusLine();
			GoogleResult googleResult = EMPTY_GOOGLE_RESULT;
			logger.debug(statusLine.toString());
			if (statusLine.getStatusCode() == 200) {
				Reader reader = new InputStreamReader(arg0.getEntity()
						.getContent());
				googleResult = new Gson().fromJson(reader, GoogleResult.class);
			} else {
				arg0.getEntity().getContent();
			}
			return googleResult;
		}

	}

	/**
	 * Classe che modella i risultati della ricerca effettuata su Google
	 * @author Simone 
	 * */
	static class GoogleResult {

		private ResponseData responseData;

		public ResponseData getResponseData() {
			return responseData;
		}

		public void setResponseData(ResponseData responseData) {
			this.responseData = responseData;
		}

		static class ResponseData {
			private List<Result> results;

			public List<Result> getResults() {
				return results;
			}

			public void setResults(List<Result> results) {
				this.results = results;
			}

		}

		/**
		 * Classe che modella il singolo risultato della ricerca
		 * @author Simone
		 * */
		static class Result {

			private String title;
			private String url;

			public String getUrl() {
				return url;
			}

			public void setUrl(String url) {
				this.url = url;
			}

			public String getTitle() {
				return title;
			}

			public void setTitle(String title) {
				this.title = title;
			}

		}

	}

}
