package it.roma.sapienza.snippetsdownloader;

import it.roma.sapienza.snippetsdownloader.utils.AnchorRecognizer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;


/**
 * Test d'unit� della classe {@link TestAnchorRecognizer}
 * @author Simone
 *
 */
public class TestAnchorRecognizer {
	
	/**
	 * test semplice 
	 */
	@Test
	public void testRecognize(){
		AnchorRecognizer underTest = new AnchorRecognizer();
		List<String> result = underTest.recognize("<a href=\"http://www.google.com\" >Google</a>");
		Assert.assertNotNull(result);
		
		Assert.assertEquals(1, result.size());
		
		Assert.assertEquals(Collections.singletonList("http://www.google.com"), result);
	}
	
	
	/**
	 * 2 link sulla stessa linea
	 */
	@Test
	public void testMultipleRecognize(){
		AnchorRecognizer underTest = new AnchorRecognizer();
		List<String> result = underTest.recognize("<ul><li><a href=\"http://www.google.com\" >Google</a></li><li><a href=\"http://www.yahoo.it\" >Yahoo IT</a></li></ul>");
		Assert.assertNotNull(result);
		
		Assert.assertEquals(2, result.size());
		
		Assert.assertEquals(Arrays.asList("http://www.google.com","http://www.yahoo.it"), result);
	}

}
