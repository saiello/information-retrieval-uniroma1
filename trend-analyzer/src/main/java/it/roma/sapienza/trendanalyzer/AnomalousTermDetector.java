package it.roma.sapienza.trendanalyzer;

import it.roma.sapienza.trendanalyzer.analyzers.Analyzer;
import it.roma.sapienza.trendanalyzer.analyzers.chauvenet.ChauvenetAnalyzer;
import it.roma.sapienza.trendanalyzer.trend.Trend;
import it.roma.sapienza.trendanalyzer.trend.TrendDownloader;
import it.roma.sapienza.trendanalyzer.trend.TrendParser;

import java.io.IOException;
import java.io.InputStream;
/**
 * 
 * 
 * */
public class AnomalousTermDetector {

	private Analyzer analyzer;
	private TrendDownloader downloader;
	private TrendParser trendParser;
	
	/**
	 * Costruttore di default
	 */
	public AnomalousTermDetector() {
		this.downloader = new TrendDownloader();
		this.trendParser = new TrendParser(Constants.LOCALE);
		this.analyzer = new ChauvenetAnalyzer();
	}
	
	
	/**
	 * 
	 * @param term - il termine per cui effettuare l'analisi
	 * @return - l'esito dell'analisi
	 * @throws AnomalyDetectionException - se il download del trend non pu� essere eseguito
	 */
	public boolean isAnomalous(String term) throws AnomalyDetectionException{
		try {
			InputStream stream = this.downloader.downloadStats(term);
			Trend trend = this.trendParser.parse(stream);
			System.out.println(trend);
			this.downloader.close();
			return analyzer.isAnomalous(trend);
		} catch (IOException e) {
			throw new AnomalyDetectionException(e);
		}
		
	}
	
	/**
	 * 
	 * 
	 * */
	public class AnomalyDetectionException extends Exception{
		/**
		 * 
		 */
		private static final long serialVersionUID = -5406644413971847422L;

		/**
		 * @param t - la causa
		 */
		public AnomalyDetectionException(Throwable t) {
			super(t);
		}
	}
	
}
