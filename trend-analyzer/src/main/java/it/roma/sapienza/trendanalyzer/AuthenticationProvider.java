package it.roma.sapienza.trendanalyzer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Simone
 *
 */
public class AuthenticationProvider {
	
	private static final String AUTHENTICATION_URL = "https://www.google.com/accounts/ClientLogin?accountType=GOOGLE&Email=%s&Passwd=%s&service=trendspro&source=test-test-v1";
	
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationProvider.class);
	
	private static final AuthenticationProvider instance = new AuthenticationProvider();
	
	/**
	 * @return AuthenticationProvider
	 */
	public static AuthenticationProvider instance(){
		return instance;
	}
	
	private String token;
	private String user;
	private String pass;

	private AuthenticationProvider() {
		user = "ihoursync@gmail.com";
		pass = "tirocinio";
	}
	
	/**
	 * @return String il token di autenticazione
	 */
	public synchronized String getToken() {
		if(token==null)
			token = fetchNewToken();
		return token;
	}
	
	/**
	 * 
	 */
	public synchronized void reset(){
		token = null;
	}


	private String fetchNewToken() {
		String out = null;
		try{
			logger.info(String.format("Fetching new authentication token [ user: %s ]",user));
			String url = String.format(AUTHENTICATION_URL,user,pass);
			
		    URLConnection uconn = new URL(url).openConnection();
		   
		    BufferedReader in = new BufferedReader(new InputStreamReader(uconn.getInputStream()));
		    String inputLine;
		    
		    int i = 0;
		    String [] gAuth = new String[3];
		    
		    while ((inputLine = in.readLine()) != null) {
		    	gAuth[i] = inputLine;
		    	i++;
		    }
		    in.close();  
		    
		   out = gAuth[2];
		   
		}catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}

}
