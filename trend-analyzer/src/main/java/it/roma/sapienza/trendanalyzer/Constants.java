package it.roma.sapienza.trendanalyzer;

import java.util.Locale;

/**
 * Classe contenente alcuni costanti
 * @author Simone
 */
public class Constants {
	
	/**
	 * Numero di termini per cui effettuare il download dei trend contemporaneamente
	 */
	public static final int MAX_TERMS_NUMBER_PER_DOWNLOAD = 1;
	
	/**
	 * Locale da utilizzare in fase di parsing del file delle statistiche
	 */
	public static final Locale LOCALE = Locale.ITALIAN;
	
	/**
	 * Numero di Thread paralleli 
	 */
	public static final int MAX_POOL_THREAD_SIZE = 1;
	
	/**
	 * Limite di retry per i download che falliscono
	 */
	public static final int DOWNLOAD_RETRY_LIMIT = 20;
	
	/**
	 * Timeout per il retry dei download che falliscono
	 */
	public static final int DOWNLOAD_RETRY_TIMEOUT = 5;

	
	
}
