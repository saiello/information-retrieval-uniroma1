package it.roma.sapienza.trendanalyzer;

import it.roma.sapienza.snippetsdownloader.SnippetCollector;
import it.roma.sapienza.snippetsdownloader.SnippetFileWriter;
import it.roma.sapienza.snippetsdownloader.google.GoogleSnippetCollector;
import it.roma.sapienza.trendanalyzer.AnomalousTermDetector.AnomalyDetectionException;
import it.roma.sapienza.trendanalyzer.trend.TermSet;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe di prova per mostrare il funzionamento delle librerie
 * @author Simone
 *
 */
public class Main {
	
	private static final Logger logger = LoggerFactory.getLogger(Main.class);
	
	private static final String USAGE_MESSAGE = "Usage: \n\t run.sh input.txt [-o output.txt]\n\t run.bat input.txt [-o output.txt]";
	
	/**
	 * @param args - argomenti applicativi
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		if(args.length != 1 && args.length != 3){
			System.out.println(USAGE_MESSAGE);
			System.exit(1);
		}
		
		String inFile = args[0];
		String outFile = "output.txt";
		
		if(args.length >1){
			if(args[1].equals("-o")){
				outFile = args[2];	
			}
		}
		FileReader fileReader = null;
		try{
			fileReader = new FileReader(inFile);
		}catch (FileNotFoundException e) {
			System.out.println("File: " + inFile + ", does not exist.");
			System.exit(1);
		}
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		List<String> terms = new ArrayList<String>();
		String line;
		while((line = bufferedReader.readLine())!=null){
			terms.add(line);
		}
		
		//Collection<String> terms = Arrays.asList("spending review","mario monti","europei","spread","android","windows surface","samsung galaxy");
//		Collection<String> terms = Arrays.asList("vacanze");
		
		TermSet termSet = new TermSet(Constants.MAX_TERMS_NUMBER_PER_DOWNLOAD, terms);
		
		long start = System.currentTimeMillis();
		
		
		//TODO substitute with a thread factory
		List<Thread> pool = new ArrayList<Thread>();
		
		SnippetFileWriter snippetWriter = new SnippetFileWriter(outFile);

		for (int i = 0; i < Constants.MAX_POOL_THREAD_SIZE; i++) {
			/*
			 * TrendDownloader building.
			 * Inject the termSet, Analyzer and a SnippetCollector
			 * */
			GoogleCrawler trendDownloader = new GoogleCrawler(termSet, new GoogleSnippetCollector(snippetWriter));
			
			Thread t = new Thread(trendDownloader,"Crawler-"+i);
			t.start();
			pool.add(t);
		}
		
		//join to all threads
		try{
			for (Thread thread : pool) {
				thread.join();
			}
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//close resource
		try {
			snippetWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		logger.info("FINISHED in "+ (System.currentTimeMillis() - start) + " milliseconds");
	}
	
	
	static class GoogleCrawler implements Runnable{
		
		private final TermSet terms;
		private final SnippetCollector[] collectors;
		private final AnomalousTermDetector anomalyDetector;
		
		GoogleCrawler(TermSet terms,SnippetCollector... collectors){
			this.terms = terms;
			this.collectors = collectors;
			this.anomalyDetector = new AnomalousTermDetector();
			
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			String[] currentTerms;
			while( (currentTerms = terms.nextTerms())!=null ){
				String term = currentTerms[0];
				try {
					if(anomalyDetector.isAnomalous(term)){
						logger.info("{} is anomalous, then collect snippet.",term);
						for (SnippetCollector collector : collectors) {
							collector.collect(term);
						}
					}
				} catch (AnomalyDetectionException e) {
					e.printStackTrace();
				}
				
			}
			
		}
	}
}
