package it.roma.sapienza.trendanalyzer.analyzers;

import it.roma.sapienza.trendanalyzer.trend.Trend;

/**
 * Classe che implementa l'interfaccia {@link Analyzer} e il pattern decorator, permettendo di comporre nuovi analyzer a partire da quelli base
 * @author Simone
 *
 */
public abstract class AbstractAnalyzer implements Analyzer {
	
	private final Analyzer previous;
	
	/**
	 * 
	 * @param previous - Analyzer precedente 
	 */
	public AbstractAnalyzer(Analyzer previous) {
		this.previous = previous;
	}
	
	/**
	 * Costruttore di default
	 */
	public AbstractAnalyzer() {
		this.previous = null;
	}

	public boolean isAnomalous(Trend trend) {
		boolean result = false;
		if(previous!=null)
			result |= previous.isAnomalous(trend);
		return result |= checkAnomalies(trend);
	}

	protected abstract boolean checkAnomalies(Trend trend);
	
}
