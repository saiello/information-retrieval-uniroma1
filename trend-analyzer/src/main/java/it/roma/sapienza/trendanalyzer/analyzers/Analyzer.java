package it.roma.sapienza.trendanalyzer.analyzers;

import it.roma.sapienza.trendanalyzer.trend.Trend;

/**
 * Interfaccia Analyzer
 * @author Simone
 *
 */
public interface Analyzer {
	
	/**
	 * Restituisce l'esito del controllo del trend.
	 * true se risulta anomalo, false altrimenti.
	 * @param trend
	 * @return boolean 
	 */
	boolean isAnomalous(Trend trend);
}
