package it.roma.sapienza.trendanalyzer.analyzers.chauvenet;


class Chauvenet {
	
	     public static final int MIN_MEMBERS = 3;  // need at least this many values in the set
	     public static final double STANDARD_CUTOFF_VALUE = 0.5;
	
	     public Chauvenet() {
	     }
	
	      /** Returns the portion of a Normal distribution that is in the "tails"
	      * of the curve beyond this standard deviation in both directions from the mean. */
	      public static double amountInTails (double stdDevsAway) {
	         return 2.0 * Ztable.getAbove(stdDevsAway);
	      }
	
	      /** Uses standard cutoff of 0.5 */
	      public static boolean reject(double stdDevsAway, int count) {
	
	         return reject(stdDevsAway, count, STANDARD_CUTOFF_VALUE);
	      }
	      /** Allows non-standard user-specified cutoff value. */
	      public static boolean reject(double stdDevsAway, int count, double cutOff) {
	
	         double inTails = amountInTails(stdDevsAway);
	         return ((inTails * count) < cutOff);
	      }
	/*
	  public static final class Tester {
	     public static void main (String args[]) {
	
	         //double data[] = {1.8, 3.8, 3.5, 3.9, 3.9, 3.4};
	         //double data[] = {46, 48, 44, 38, 45, 58, 44, 45, 43};
	    	 //double data[] = {50, 50, 50, 50, 50, 50, 100, 100};
	    	 //Mario Monti 2004 - presente
	    	 //double data[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100, 28, 22, 18, 0};
	    	 //Mario Monti 7 giorni
	    	 double data[] = {100, 98, 0, 0, 100, 98, 0, 0, 100, 98, 0, 0, 100, 98, 0, 0};
	    	 double mean = Stats.mean(data);
	         double stdDev = Stats.standardDeviation(data);
	         double stdDevsAway;
	
//	         System.out.println("mean = "+mean + "  stdDev = "+ stdDev);
	
	         for (int i = 0; i< data.length; i++ ){
	
	            stdDevsAway = (mean - data[i])/stdDev;
	
	            if (Chauvenet.reject(stdDevsAway, data.length)) {
//	               System.out.println("Reject "+data[i]);
	            } else {
//	               System.out.println("Accept "+data[i]);
	            }
	         }
	
	     }
	  }
	  */
}