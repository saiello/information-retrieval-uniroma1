package it.roma.sapienza.trendanalyzer.analyzers.chauvenet;

import it.roma.sapienza.trendanalyzer.analyzers.Analyzer;
import it.roma.sapienza.trendanalyzer.trend.Trend;
import it.roma.sapienza.trendanalyzer.util.Stats;

/**
 * Implementazione di {@link Analyzer} utilizzando il metodo di Chauvenet
 * @author Simone
 */
public class ChauvenetAnalyzer implements Analyzer{

	public boolean isAnomalous(Trend trend) {
		//Si usa il criterio di Chauvenet per stabilire se si tratta di trend anomalo
		boolean ret = false;
		byte[] p = trend.getPoints();
		double[] data = new double[p.length];
		for (int i = 0; i < p.length; i++){
			data[i] = (double)p[i];
		}
		double mean = Stats.mean(data);
        double stdDev = Stats.standardDeviation(data);
        double stdDevsAway;

        System.out.println("mean = "+mean + "  stdDev = "+ stdDev);

        for (int i = 0; i< data.length; i++ ){
        	stdDevsAway = (mean - data[i])/stdDev;
        	if (Chauvenet.reject(stdDevsAway, data.length)) {
//               System.out.print("RJ"+data[i]+" ");
               ret = true;
            } else {
//               System.out.print("AC"+data[i]+" ");
            }
         }
		return ret;
	}
}