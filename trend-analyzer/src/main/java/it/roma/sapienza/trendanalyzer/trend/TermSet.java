package it.roma.sapienza.trendanalyzer.trend;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 
 * @author Simone
 *
 */
public class TermSet {
	
	private List<String> terms;
	
	private int maxReturnedTerms;
	
	/**
	 * Costruttore 
	 * @param maxReturnedTerms - numero massimo di termini da restituire ad ogni next
	 * @param terms - Collezione di termini con cui inizializzare il set 
	 */
	public TermSet(int maxReturnedTerms, Collection<String> terms) {
		this.terms = new ArrayList<String>(terms);
		this.maxReturnedTerms = maxReturnedTerms;
	}
	
	/**
	 * Restituisce i successivi termini del set, in accordo con il limite massimo impostato
	 * @return String[]
	 */
	public String[] nextTerms(){
		String[] out = null;
		
		synchronized (terms) {
			if(!terms.isEmpty()){

				int size = (terms.size()<maxReturnedTerms)?terms.size():maxReturnedTerms;
				
				out = new String[size];
				
				for (int i = 0; i < out.length; i++) {
					out[i] = terms.remove(0);
				}
			}
		}
		
		return out;
	}

}
