package it.roma.sapienza.trendanalyzer.trend;

import java.util.Arrays;
import java.util.List;

/**
 * Classe che rappresenta il trend di ricerca di un termine
 * @author Simone
 *
 */
public class Trend {
	
	
	private final String term;
	
	private byte[] points;
	
	/**
	 * 
	 * @param term - termine rappresentato dal trend
	 */
	public Trend(String term){
		this.term = term;
	}
	
	@Override
	public String toString() {
		return "Trend [term=" + term + ", points=" + Arrays.toString(points)  + "]";
	}


	/**
	 * @param list - lista di byte rappresentanti i valori del trend di ricerca 
	 */
	public void setPoints(List<Byte> list) {
		points = new byte[list.size()];
		for (int i = 0; i < points.length; i++) {
			points[i] = list.remove(0);
		}
	}
	
	/**
	 * @return String - il termine rappresentato
	 */
	public String term(){
		return term;
	}
	
	/**
	 * 
	 * @return byte[] i valori del trend di ricerca
	 */
	public byte[] getPoints(){
		return points;
	}
	
}
