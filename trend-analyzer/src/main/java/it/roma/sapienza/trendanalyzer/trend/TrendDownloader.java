package it.roma.sapienza.trendanalyzer.trend;

import it.roma.sapienza.trendanalyzer.AuthenticationProvider;
import it.roma.sapienza.trendanalyzer.Constants;
import it.roma.sapienza.trendanalyzer.util.StringUtil;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe responsabile di effettuare il download dei trend di ricerca da
 * GoogleTrend
 * 
 * @author Simone
 */
public class TrendDownloader implements Closeable {

	private static Logger logger = LoggerFactory
			.getLogger(TrendDownloader.class);

	private static final String BASE_URL = "http://www.google.com/trends/trendsReport?hl=it&q=%s&content=1&export=1";

	private final AuthenticationProvider authTokenProvider;

	private DefaultHttpClient httpclient;

	/**
	 * Costruttore no-arg
	 */
	public TrendDownloader() {
		this.authTokenProvider = AuthenticationProvider.instance();
	}

	/**
	 * 
	 * @param terms
	 *            termini per cui effettuare il download delle statistiche di
	 *            ricerca
	 * @return l'input stream corrispondente al file csv rappresentante i trend
	 *         di ricerca per i termini forniti in input
	 * @throws IOException
	 *             -
	 */
	public InputStream downloadStats(String... terms) throws IOException {

		logger.info("Download stats for terms: {}", Arrays.toString(terms));
		InputStream stream = null;

		long start = System.currentTimeMillis();

		int tentativo = 0;

		while (tentativo++ <= Constants.DOWNLOAD_RETRY_LIMIT) {
			try {
				httpclient = new DefaultHttpClient();
				HttpGet httpget = prepareHttpGet(terms);
				httpget.addHeader("Authorization", "GoogleLogin "
						+ authTokenProvider.getToken());

				HttpResponse response = httpclient.execute(httpget);

				StatusLine statusLine = response.getStatusLine();

				logger.debug(statusLine.toString());

				if (statusLine.getStatusCode() == 200) {
					stream = response.getEntity().getContent();
					logger.info("Download eseguito in {}",
							System.currentTimeMillis() - start);
					break;
				} else {
					logger.info(
							"Tentativo di download n. {}, fallito. Nuovo tentativo fra {} secondi.",
							tentativo, Constants.DOWNLOAD_RETRY_TIMEOUT);
					TimeUnit.SECONDS.sleep(Constants.DOWNLOAD_RETRY_TIMEOUT);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				throw new IOException(e);
			}
		}
		return stream;
	}

	private HttpGet prepareHttpGet(String... terms) {

		if (terms.length > Constants.MAX_TERMS_NUMBER_PER_DOWNLOAD) {

			logger.warn("Too many terms. Trend analysis will be downloaded just for %d of them.");
			terms = Arrays.copyOf(terms,
					Constants.MAX_TERMS_NUMBER_PER_DOWNLOAD);
		}

		String termsArgument = StringUtil.join(terms, ",").replaceAll(" ",
				"%20");

		String url = String.format(BASE_URL, termsArgument, "today%201-m");
		logger.info("URL: "+url);
		HttpGet httpget = new HttpGet(url);

		return httpget;
	}

	public void close() throws IOException {
		httpclient.getConnectionManager().shutdown();
	}

}
