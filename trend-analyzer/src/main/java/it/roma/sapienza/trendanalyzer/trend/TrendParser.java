package it.roma.sapienza.trendanalyzer.trend;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * 
 * @author Simone
 *
 */
public class TrendParser {


	/**
	 * 
	 * @author Simone
	 *
	 */
	private enum TrendType{
		GIORNALIERO,
		MENSILE,
		SETTIMANALE;
		
		private final String headerKey,prefixKey;
		
		TrendType(){
			this.headerKey = String.format("builder.constants.trend.%s.header",this.name().toLowerCase());
			this.prefixKey = String.format("builder.constants.trend.%s.prefix",this.name().toLowerCase());
		}
		
		String headerKey(){
			return this.headerKey;
		}
		
		String prefixKey(){
			return this.prefixKey;
		}
		
	}
	
	private static final String PROPERTIES_FILE_NAME = "trend-builder_%s.properties";

	private static final String BASE_PATH = "trend_builder_settings";
	
	private Properties properties;
	
	private final String separator;
	
	/**
	 * Costruttore di default, utilizza Locale.ITALIAN 
	 * 
	 * */
	public TrendParser(){
		this(Locale.ITALIAN);
	}

	/**
	 * Costruttore
	 * @param locale - Locale fornito
	 */
	public TrendParser(Locale locale){
		this.properties = new Properties();
		String propertiesName = String.format(PROPERTIES_FILE_NAME, locale.getLanguage());
//		String resourceName = BASE_PATH+System.getProperty("file.separator")+propertiesName;
		String resourceName = propertiesName;
		InputStream resourceStream = this.getClass().getClassLoader().getResourceAsStream(resourceName);
		
		if(resourceStream==null){
			throw new IllegalStateException("Resource "+ resourceName +" does not exist.");
		}
		
		try {
			this.properties.load(resourceStream);
			this.separator = property("builder.constants.trend.data.separator");
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	
	

	/**
	 * @param inputStream - l'input stream 
	 * @return - Trend
	 */
	@SuppressWarnings("unchecked")
	public Trend parse(InputStream inputStream){
		
		Trend[] trends = null;
		List<Byte>[] values = null;
		
		if(inputStream==null)
			throw new IllegalArgumentException("The reader can not be null!");
		

		TrendType type = null;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			while((line = br.readLine())!=null){
				
				if(type!=null&&line.trim().equals("")){
					
					for (int i = 0; i < trends.length; i++) {
						trends[i].setPoints(values[i]);
					}
					
					//end reading
					break;
				}else if(type!=null){

					//reading next values 
					incrementTrends(values,line.substring(property(type.prefixKey()).length()));
				
				}else if( (type = calculateTrendType(line)) != null){
					//init trends and values
					trends = initTrends(line.substring(property(type.headerKey()).length()));
					values = new ArrayList[trends.length];
					for (int i = 0; i < values.length; i++) {
						values[i] = new ArrayList<Byte>();
					}
					//then start reading payload
				}
			}	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(type == null)
			throw new IllegalStateException("No trend read.");
		
		return trends[0];
	}

	
	private void incrementTrends(List<Byte>[] trendValues,String line) {
		String[] values = line.split(separator);
		for (int i = 0; i < values.length; i++) {
			if(!values[i].trim().equals("")&&values[i].trim().matches("(\\d)*"))
				trendValues[i].add(Byte.parseByte(values[i]));
		}
	}


	private Trend[] initTrends(String line) {
		String[] terms = line.split(separator);
		List<Trend> trendList = new ArrayList<Trend>();
		for (String term : terms) {
			trendList.add(new Trend(term));
		}
		return trendList.toArray(new Trend[]{});
	}


	private TrendType calculateTrendType(String line) {
		for (TrendType type : TrendType.values()) {
			if(line.startsWith(property(type.headerKey()))) return type;
		}
		return null;
	}
	
	private String property(String key){
		String property = properties.getProperty(key);
		if(property == null)
			throw new IllegalStateException("property [ " + key + " ] must be defined.");
		
		return property;
	}
	
	public static void main(String[] args) {
		Properties props = System.getProperties();
		for (Object k : props.keySet()) {
			System.out.println(k+" - "+ System.getProperty(k.toString()));
		}
	}

}
