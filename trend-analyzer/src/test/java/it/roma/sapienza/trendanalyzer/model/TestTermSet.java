package it.roma.sapienza.trendanalyzer.model;

import it.roma.sapienza.trendanalyzer.trend.TermSet;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * Test d'unit� della classe {@link TermSet}
 * @author Simone
 *
 */
public class TestTermSet extends TestCase{

	
	private static final int MAX_RETURNED_LIMIT_TEST = 5;


	/**
	 * test semplice: 1 termine
	 */
	@Test
	public void testTermSet(){
		
		Collection<String> terms = Arrays.asList("term1");
		
		TermSet underTest = new TermSet(MAX_RETURNED_LIMIT_TEST,terms);
		
		String[] result = underTest.nextTerms();
		
		assertNotNull(result);
		
		assertEquals(1, result.length);
		
		assertEquals("term1", result[0]);
		
		result = underTest.nextTerms();
		
		assertNull(result);
		
	}
	
	
	/**
	 * 8 termini, 5 max result
	 */
	@Test
	public void testTermSetManyTerms(){
		
		Collection<String> terms = Arrays.asList("term1","term2","term3","term4"
				,"term5","term6","term7","term8");
		
		TermSet underTest = new TermSet(MAX_RETURNED_LIMIT_TEST,terms);
		
		String[] result = underTest.nextTerms();
		
		assertNotNull(result);
		
		assertEquals(5, result.length);
		
		if(!Arrays.equals(result, new String[]{"term1","term2","term3","term4","term5"}))
			fail("Not same values");
		
		
		result = underTest.nextTerms();
		
		assertEquals(3, result.length);
		
		if(!Arrays.equals(result, new String[]{"term6","term7","term8"}))
			fail("Not same values");
		
		result = underTest.nextTerms();
		
		assertNull(result);
	}
}
