/**
 * 
 */
package it.roma.sapienza.trendanalyzer.trend;

import java.io.InputStream;

import static junit.framework.Assert.*;

import org.junit.Test;

/**
 * @author Simone
 *
 */
public class TestTrendParser {

	/**
	 * Test method for {@link it.roma.sapienza.trendanalyzer.trend.TrendParser#parse(java.io.InputStream)}.
	 */
	@Test
	public void testParseTrendGiornaliero() {
		InputStream inputStream = TrendParser.class.getClassLoader().getResourceAsStream("giornaliero.csv");
		Trend trend = new TrendParser().parse(inputStream);
		assertNotNull(trend);
		checkTrend(trend,"europei",29,18,63);
	}
	
	
	/**
	 * Test method for {@link it.roma.sapienza.trendanalyzer.trend.TrendParser#parse(java.io.InputStream)}.
	 */
	@Test
	public void testParseTrendMensile() {
		InputStream inputStream = TrendParser.class.getClassLoader().getResourceAsStream("mensile.csv");
		Trend trend = new TrendParser().parse(inputStream);
		assertNotNull(trend);
		checkTrend(trend,"spending review",107,2,13);
	}
	
	/**
	 * Test method for {@link it.roma.sapienza.trendanalyzer.trend.TrendParser#parse(java.io.InputStream)}.
	 */
	@Test
	public void testParseTrendSettimanale() {
		InputStream inputStream = TrendParser.class.getClassLoader().getResourceAsStream("settimanale.csv");
		Trend trend = new TrendParser().parse(inputStream);
		assertNotNull(trend);
		checkTrend(trend,"mario monti",462,0,3);
	}
	
	/**
	 * Test method for {@link it.roma.sapienza.trendanalyzer.trend.TrendParser#parse(java.io.InputStream)}.
	 */
	@Test(expected=IllegalStateException.class)
	public void testParseTrendFake() {
		InputStream inputStream = TrendParser.class.getClassLoader().getResourceAsStream("fake.csv");
		new TrendParser().parse(inputStream);
	}
	
	
	private static void checkTrend(Trend trend,String name, int size, int first,int last){
		//trend name
		assertEquals(name, trend.term());
		//length
		assertEquals(size, trend.getPoints().length);
		//first value
		assertEquals(first, trend.getPoints()[0]);
		//last value
		assertEquals(last, trend.getPoints()[trend.getPoints().length-1]);
	}
}
